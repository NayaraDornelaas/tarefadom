/*funcao para enviar a mensagem para a caixa*/
var secao = document.querySelector("#messages_sent");
function enviarMensagem(){
    let input = document.querySelector(".input_messages");
    let escopo = document.createElement("div");
    let valor = document.createElement("p");
    valor.innerText = input.value;
    escopo.appendChild(valor);
    secao.value += input.value + "\n";
 
}

/* funcao para excluir mensagem*/
function excluirMensagem(){
    secao.value="";

}

let btn_enviar=document.querySelector("#btn_enviar");
btn_enviar.addEventListener("click",()=>{enviarMensagem()});

let btn_excluir = document.querySelector("#btn_excluir");
btn_excluir.addEventListener("click", ()=>{excluirMensagem()});